﻿namespace OpenData.Warsaw19115Api
{
    using Newtonsoft.Json;
    using OpenData.Warsaw19115Api.Responses;
    using RestSharp;
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    public class Warsaw19115ApiClient : IWarsaw19115ApiClient
    {
        private static readonly string ApiId = "28dc65ad-fff5-447b-99a3-95b71b4a7d1e";


        private readonly IRestClient _restClient;
        private readonly string _apiKey;

        public Warsaw19115ApiClient(IRestClient restClient, string apiUrl, string apiKey)
        {
            if (string.IsNullOrWhiteSpace(apiKey))
                throw new ArgumentOutOfRangeException(nameof(apiKey));

            if (string.IsNullOrWhiteSpace(apiUrl))
                throw new ArgumentOutOfRangeException(nameof(apiUrl));

            _apiKey = apiKey;
            _restClient = restClient ?? throw new ArgumentNullException(nameof(restClient));
            _restClient.BaseUrl = new Uri(apiUrl);
        }

        public IEnumerable<Notification> GetNotificationsByDate(DateTime dateFrom, DateTime dateTo)
        {
            var request = CreateGetNotificationsByDateRequest(dateFrom, dateTo);
            var response = _restClient.Execute(request);
            var rootResult = JsonConvert.DeserializeObject<RootGetNotificationsResponse>(response.Content);
            ThrowExceptionIfResponseHasErrors(rootResult);
            return rootResult.Result.Result.Notifications;
        }
        
        public async Task<IEnumerable<Notification>> GetNotificationsByDateAsync(DateTime dateFrom, DateTime dateTo)
        {
            var request = CreateGetNotificationsByDateRequest(dateFrom, dateTo);
            var response = await _restClient.ExecuteTaskAsync(request);
            var rootResult = JsonConvert.DeserializeObject<RootGetNotificationsResponse>(response.Content);
            ThrowExceptionIfResponseHasErrors(rootResult);
            return rootResult.Result.Result.Notifications;
        }

        private static void ThrowExceptionIfResponseHasErrors(RootGetNotificationsResponse rootResult)
        {
            if (!rootResult.Result.Success)
                throw new Warsaw19115ApiException(rootResult.Result.Result.ResponseCode, rootResult.Result.Result.ResponseDesc);
        }

        private IRestRequest CreateGetNotificationsByDateRequest(DateTime dateFrom, DateTime dateTo)
        {
            long dateFromUnix = new DateTimeOffset(dateFrom).ToUnixTimeMilliseconds();
            long dateToUnix = new DateTimeOffset(dateTo).ToUnixTimeMilliseconds();

            var request = new RestRequest("19115store_getNotificationsForDate", Method.GET);
            request.AddParameter("id", ApiId);
            request.AddParameter("dateFrom", dateFromUnix);
            request.AddParameter("dateTo", dateToUnix);
            request.AddParameter("apikey", _apiKey);
            return request;
        }
    }
}
