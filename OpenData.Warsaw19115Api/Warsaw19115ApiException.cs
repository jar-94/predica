﻿namespace OpenData.Warsaw19115Api
{
    using System;

    public class Warsaw19115ApiException : Exception
    {
        public Warsaw19115ApiException(string responseCode, string responseDescription)
            : base($"Invalid API response. Code: {responseCode} Description: {responseDescription}")
        {
            ResponseCode = responseCode;
            ResponseDescription = ResponseDescription;
        }

        public string ResponseCode { get; private set; }
        public string ResponseDescription { get; private set; }
    }
}
