﻿namespace OpenData.Warsaw19115Api.Responses
{
    using Newtonsoft.Json;

    internal class GetNotificationsResponse
    {
        [JsonProperty("success")]
        public bool Success { get; private set; }

        [JsonProperty("result")]
        public NotificationsResponse Result { get; private set; }
    }
}
