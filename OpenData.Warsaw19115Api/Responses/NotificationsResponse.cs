﻿namespace OpenData.Warsaw19115Api.Responses
{
    using Newtonsoft.Json;
    using System.Collections.Generic;

    internal class NotificationsResponse
    {
        [JsonProperty("notifications")]
        public IList<Notification> Notifications { get; private set; }

        [JsonProperty("responseDesc")]
        public string ResponseDesc { get; private set; }

        [JsonProperty("responseCode")]
        public string ResponseCode { get; private set; }
    }
}
