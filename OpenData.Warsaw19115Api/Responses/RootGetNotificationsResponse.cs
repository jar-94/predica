﻿namespace OpenData.Warsaw19115Api.Responses
{
    using Newtonsoft.Json;

    internal class RootGetNotificationsResponse
    {
        [JsonProperty("result")]
        public GetNotificationsResponse Result { get; private set; }
    }
}
