﻿namespace OpenData.Warsaw19115Api.Responses
{
    using Newtonsoft.Json;
    using System;

    public class Notification
    {
        [JsonProperty("category")]
        public string Category { get; private set; }

        [JsonProperty("city")]
        public string City { get; private set; }

        [JsonProperty("subcategory")]
        public string Subcategory { get; private set; }

        [JsonProperty("district")]
        public string District { get; private set; }

        [JsonProperty("aparmentNumber")]
        public string AparmentNumber { get; private set; }

        [JsonProperty("street2")]
        public string Street2 { get; private set; }

        [JsonProperty("notificationType")]
        public string NotificationType { get; private set; }

        [JsonProperty("siebelEventId")]
        public string SiebelEventId { get; private set; }

        [JsonProperty("source")]
        public string Source { get; private set; }

        [JsonProperty("yCoordOracle")]
        public double YCoordOracle { get; private set; }

        [JsonProperty("street")]
        public string Street { get; private set; }

        [JsonProperty("deviceType")]
        public string DeviceType { get; private set; }

        [JsonProperty("xCoordOracle")]
        public double XCoordOracle { get; private set; }

        [JsonProperty("notificationNumber")]
        public string NotificationNumber { get; private set; }

        [JsonProperty("yCoordWGS84")]
        public double YCoordWGS84 { get; private set; }

        [JsonProperty("event")]
        public string Event { get; private set; }

        [JsonProperty("xCoordWGS84")]
        public double XCoordWGS84 { get; private set; }
    }
}
