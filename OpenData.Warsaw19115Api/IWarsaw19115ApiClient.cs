﻿namespace OpenData.Warsaw19115Api
{
    using OpenData.Warsaw19115Api.Responses;
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    public interface IWarsaw19115ApiClient
    {
        IEnumerable<Notification> GetNotificationsByDate(DateTime dateFrom, DateTime dateTo);
        Task<IEnumerable<Notification>> GetNotificationsByDateAsync(DateTime dateFrom, DateTime dateTo);
    }
}
