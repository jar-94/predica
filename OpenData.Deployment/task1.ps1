Param(
    [string]$ResourceGroupName,
    [string[]]$ResourceTypes = @('Microsoft.Sql/Servers/Databases', 'Microsoft.Web/sites')
)

if(-not($ResourceGroupName)) { Throw "You must supply a value for -ResourceGroupName" }

foreach ($resourceType in $ResourceTypes)
{
    $resources = Get-AzureRmResource -ResourceGroupName $ResourceGroupName -ResourceType $resourceType
    foreach($res in $resources)
    {
        Remove-AzResource -ResourceId $res.ResourceId -Force
    }
}