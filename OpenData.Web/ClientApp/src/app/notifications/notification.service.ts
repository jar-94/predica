import { Injectable } from '@angular/core';
import { HttpClient, HttpParams  } from '@angular/common/http';
import { DateRange } from './date-range';
import { Notification } from './notification';
import { Observable } from 'rxjs';
import { NgbDate } from '@ng-bootstrap/ng-bootstrap';
import { NotificationAttrs } from './notification-attrs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class NotificationService {

  constructor(private http: HttpClient) { }

  getNotifications(dateRange: DateRange): Observable<Notification[]> {
    let params = new HttpParams();
    params = params.append('dateFrom', this.getDate(dateRange.fromDate));
    params = params.append('dateTo', this.getDate(dateRange.toDate));
    return this.http.get<NotificationAttrs[]>('/api/Notifications/GetNotificationsByDate', {params: params}).pipe(
      map((data) => data.map((notificationAttrs) => new Notification(notificationAttrs)))
    );
  }

  private getDate(date: NgbDate): string {
    return date.year + '.' + date.month + '.' + date.day;
  }
}
