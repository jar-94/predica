import { NgbDate } from '@ng-bootstrap/ng-bootstrap';

export class DateRange {
  fromDate: NgbDate;
  toDate: NgbDate;

  constructor(fromDate: NgbDate, toDate: NgbDate){
    this.fromDate = fromDate;
    this.toDate = toDate;
  }
}
