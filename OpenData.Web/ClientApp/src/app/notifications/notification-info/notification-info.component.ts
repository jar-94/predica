import { Component, Input, OnInit } from '@angular/core';
import { Notification } from '../notification';

@Component({
  selector: 'notification-info',
  templateUrl: './notification-info.component.html',
  styleUrls: ['./notification-info.component.css']
})
export class NotificationInfoComponent implements OnInit {
  @Input() notification: Notification;

  constructor() { }

  ngOnInit() {
  }

}
