export interface NotificationAttrs {
  category: string;
  notificationType: string;
  district: string;
}
