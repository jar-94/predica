import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NotificationsRoutingModule } from './notifications-routing.module';
import { PlotsHubComponent } from './plots-hub/plots-hub.component';
import { DatepickerRangeComponent } from './datepicker-range/datepicker-range.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgxSpinnerModule } from 'ngx-spinner';
import { NotificationInfoComponent } from './notification-info/notification-info.component';

@NgModule({
  declarations: [PlotsHubComponent, DatepickerRangeComponent, NotificationInfoComponent],
  imports: [
    CommonModule,
    NotificationsRoutingModule,
    NgbModule,
    NgxSpinnerModule
  ],
  exports: [
    PlotsHubComponent
  ]
})
export class NotificationsModule { }
