import { NotificationAttrs } from './notification-attrs';

export class Notification {
  category: string;
  notificationType: string;
  district: string;

  constructor(attrs: Partial<NotificationAttrs> = {}) {
    this.category = attrs.category;
    this.notificationType = attrs.notificationType;
    this.district = attrs.district;
  }
}
