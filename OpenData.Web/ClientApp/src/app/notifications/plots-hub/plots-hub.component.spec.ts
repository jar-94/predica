import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlotsHubComponent } from './plots-hub.component';

describe('PlotsHubComponent', () => {
  let component: PlotsHubComponent;
  let fixture: ComponentFixture<PlotsHubComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlotsHubComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlotsHubComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
