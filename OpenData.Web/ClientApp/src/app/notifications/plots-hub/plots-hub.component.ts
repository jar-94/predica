import { Component, OnInit } from '@angular/core';
import { DateRange } from '../date-range';
import { NgxSpinnerService } from 'ngx-spinner';
import { NotificationService } from '../notification.service';
import { Notification } from '../notification';

@Component({
  selector: 'plots-hub',
  templateUrl: './plots-hub.component.html',
  styleUrls: ['./plots-hub.component.css']
})
export class PlotsHubComponent implements OnInit {
  notifications: Notification[]

  constructor(private spinner: NgxSpinnerService, private notificationService: NotificationService) { }

  ngOnInit() {
  }

  reloadNotifications(dateRange: DateRange) {
    this.spinner.show();

    this.notificationService.getNotifications(dateRange).subscribe({
      next: (notifications) => {
        console.log(notifications.length);
        this.notifications = notifications;
        this.spinner.hide();
      },
      error: () => {
        alert('Nie udało się pobrać alertów')
        this.spinner.hide();
      },
    });
  }
}
