﻿namespace OpenData.Web.ViewModels
{
    public class Notification
    {
        public Notification()
        {

        }

        public Notification(string category, string notificationType, string district)
        {
            Category = category;
            NotificationType = notificationType;
            District = district;
        }

        public string Category { get; set; }
        public string NotificationType { get; private set; }
        public string District { get; set; }
    }
}
