﻿namespace OpenData.Web.Controllers
{
    using Microsoft.AspNetCore.Mvc;
    using OpenData.Warsaw19115Api;
    using OpenData.Web.ViewModels;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    [ApiController]
    [Route("api/[controller]/[action]")]
    public class NotificationsController : Controller
    {
        private readonly IWarsaw19115ApiClient _warsaw19115ApiClient;

        public NotificationsController(IWarsaw19115ApiClient warsaw19115ApiClient)
        {
            _warsaw19115ApiClient = warsaw19115ApiClient;
        }

        [HttpGet]
        public async Task<IEnumerable<Notification>> GetNotificationsByDate([FromQuery] DateTime dateFrom, [FromQuery] DateTime dateTo)
        {
            var notifications = await _warsaw19115ApiClient.GetNotificationsByDateAsync(dateFrom, dateTo);
            return notifications.Select(n => new Notification(n.Category, n.NotificationType, n.District)).ToList();
        }
    }
}
